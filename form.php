<?php
    // db_name: 966438
    // db_user: 966438 
    // db_pass: account pass 
    // db_host: localhost

    // Connect to DB
    $servername = "localhost";
    $username   = "root";
    $password   = "root";
    $dbname     = "cdmo";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $conn->prepare("INSERT INTO applicants (name, number, email, job, date, deadline, code)
        VALUES (:name, :number, :email, :job, :date, :deadline, :code)");

        $name     = $_POST['name'];
        $number   = $_POST['number'];
        $email    = $_POST['email'];
        $job      = $_POST['job'];
        $date     = $_POST['date'];
        $deadline = $_POST['expected'];
        $code     = $_POST['code'];

        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':number', $number);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':job', $job);
        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':deadline', $deadline);
        $stmt->bindParam(':code', $code);

        $stmt->execute();

        echo "New record created successfully.";
    }
    
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }
    
    $conn = null;
?>