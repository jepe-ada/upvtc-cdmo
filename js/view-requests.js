(function($) {
    var jqxhr = $.get( "./viewRequests.php", function(data) {
        data = JSON.parse(data);

        // Iterate through all requests
        for (var i = 0; i < data.length; i++) {

            // Create table row
            var item = $('<tr></tr>');

            // Iterate through columns
            for (var val in data[i]) {
                if (val == 'id') {
                    continue;
                }

                // Create table cell
                $(item).append('<td>' + data[i][val] + '</td>');
            }

            // Append table row
            $('table#requests').append(item);
        }

    }).fail(function(thing) {
        console.log(thing);
    });
})(window.jQuery);