(function($) {
    // Contains the data to be sent to server.
    var formData = {};

    // Adds click listener to the submit button.
    $('#submit-btn').on('click.submit', submitAction);

    /**
     * Activated when submit button is clicked.
     * 
     * @param evt Event object 
     * @returns undefined
     */
    function submitAction(evt) {
        evt.preventDefault();
        
        // Get requester data
        formData['name']    = $('input#name').val();
        formData['number']  = $('input#number').val();
        formData['email']   = $('input#email').val();
        // formData['address'] = $('input#address').val();

        // Get job data
        formData['job']       = $('input[name="job"]:checked').val();
        // formData['place']     = $('input#where').val();
        formData['date']      = new Date();
        // formData['time']      = $('input#time').val();
        // formData['comments']  = $('textarea#comment').val();
        formData['code']      = $('input[name="code"]:checked').val();
        // formData['materials'] = $('textarea#materials').val();
        // formData['purpose']   = $('textarea#for').val();
        formData['expected']  = $('input#delivery').val();

        // dataString = JSON.stringify(formData);

        var jqxhr = $.post( "./form.php", formData, function(data) {
            console.log(data);
            alert("Yehey! You did it!");
        }).fail(function(thing) {
            console.log(thing);
        });
    }
})(window.jQuery);