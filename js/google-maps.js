function initialize() {
	var mapProp = {
		center:new google.maps.LatLng(11.247731, 125.007979),
		zoom:19,
		mapTypeId:google.maps.MapTypeId.ROADMAP
	};
	var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}

google.maps.event.addDomListener(window, 'load', initialize);