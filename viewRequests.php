<?php
    // db_name: 966438
    // db_user: 966438 
    // db_pass: account pass 
    // db_host: localhost

    // Connect to DB
    $servername = "localhost";
    $username   = "root";
    $password   = "root";
    $dbname     = "cdmo";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt    = $conn->query("SELECT * FROM applicants ORDER BY code ASC, deadline ASC");
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode($results);
    }
    
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }
    
    $conn = null;
?>